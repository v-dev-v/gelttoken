pragma solidity ^0.4.17;

import "./SafeMath.sol";
import "./GeltToken.sol";

contract Exchange {
    using SafeMath for uint;

    struct Balance {
        uint reserved;
        uint available;
    }

    mapping (address => mapping (address => Balance)) private balances;

    uint64 lastOrderId;
        
    event Deposit(address indexed token, address indexed owner, uint amount);
    event Withdraw(address indexed token, address indexed owner, uint amount);
    event NewOrder(address indexed token, address indexed owner, uint64 id, bool sell, uint price, uint amount, uint64 timestamp);
    
    modifier isToken(address token) {
        require(token != 0);
        _;
    }

    function Exchange() public {
    }
    
    function deposit() payable public {
        balances[0][msg.sender].available = balances[0][msg.sender].available.add(msg.value);
        Deposit(0, msg.sender, msg.value);
    }

    function withdraw(uint amount) payable public {
        balances[0][msg.sender].available = balances[0][msg.sender].available.sub(amount);
        msg.sender.transfer(amount);
        Withdraw(0, msg.sender, amount);
    }

    function depositToken(GeltToken token, uint amount) public {
        token.transferFrom(msg.sender, this, amount);
        balances[token][msg.sender].available = balances[token][msg.sender].available.add(amount);
        Deposit(token, msg.sender, amount);
    }

    function withdrawToken(GeltToken token, uint amount) public {
        balances[token][msg.sender].available = balances[token][msg.sender].available.sub(amount);
        token.transfer(msg.sender, amount);
        Withdraw(token, msg.sender, amount);
    }
    

    function sell(address token, uint amount, uint price) public returns (uint64) {
        balances[token][msg.sender].available = balances[token][msg.sender].available.sub(amount);
        balances[token][msg.sender].reserved = balances[token][msg.sender].reserved.add(amount);      

        balances[0][msg.sender].available = balances[0][msg.sender].available.sub(amount.mul(price));
        balances[0][msg.sender].reserved = balances[0][msg.sender].reserved.add(amount.mul(price));       

        uint64 id = ++lastOrderId;
        NewOrder(token, msg.sender, id, true, price, amount, uint64(now));
        
        return id;
    }   

    function buy(address token, uint amount, uint price) public returns (uint64) {
        balances[0][msg.sender].available = balances[0][msg.sender].available.sub(amount.mul(price));
        balances[0][msg.sender].reserved = balances[0][msg.sender].reserved.add(amount.mul(price));

        balances[token][msg.sender].available = balances[token][msg.sender].available.sub(amount);
        balances[token][msg.sender].reserved = balances[token][msg.sender].reserved.add(amount);          

        uint64 id = ++lastOrderId;
        NewOrder(token, msg.sender, id, false, price, amount, uint64(now));
        
        return id;
    }  
        
    function getBalance(address token, address trader) public constant returns (uint available, uint reserved) {
        available = balances[token][trader].available;
        reserved = balances[token][trader].reserved;
    }
}